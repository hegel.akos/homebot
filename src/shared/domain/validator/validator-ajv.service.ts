import { ValidatorService } from './validator.service';
import AJV from 'ajv';

export const validatorAjvServiceFactory = ({ schema }: { schema: object }): ValidatorService => {
  const ajv = new AJV({
    allErrors: true,
  });

  const validate = async (obj: object): Promise<boolean> => {
    const isValid = await ajv.validate(schema, obj);
    return isValid;
  };

  return { validate };
};
