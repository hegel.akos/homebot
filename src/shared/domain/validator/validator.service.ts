export interface ValidatorService {
  validate(obj: object): Promise<boolean>;
}
