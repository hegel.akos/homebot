import { Intent } from '../../intent/intent';
import { SchedulerItem } from './scheduler-item';

export const schedulerItemFactory = ({
  jobId = Intent.START_NETFLIX,
  done = false,
  options = {},
}: Partial<SchedulerItem> = {}): SchedulerItem => ({
  jobId,
  done,
  options,
});
