import { Intent, IntentOptions } from '../../intent/intent';

export type SchedulerItem = {
  jobId: Intent;
  done: boolean;
  options: IntentOptions;
};
export type SchedulerItemUpdates = Partial<Omit<SchedulerItem, 'id' | 'createdAt'>>;
