import firebase from 'firebase';
import { SchedulerItem } from './scheduler-item/scheduler-item';
import { Job } from './../job/job';

export const initJobsFirebaseScheduler = ({ dbRef, jobs }: { dbRef: firebase.database.Reference; jobs: Job[] }) => {
  const currentDate = new Date().toISOString().substring(0, 10);
  const dailyDbRef = dbRef.child(currentDate);
  dailyDbRef.on('child_added', (snapshot) => {
    const val = snapshot.val() as SchedulerItem;
    console.log(val);
    if (val.done) {
      return;
    }
    const job = jobs.find((job) => job.id === val.jobId);
    if (job) {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      void job.run(val.options || {});
      // void val.set({ done: true });
    }
  });
};
