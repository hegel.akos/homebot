export interface LampsService {
  setLamps(groups: string[], level: number): Promise<void>;
  off(groups: string[], level: number): Promise<void>;
}
