/* eslint-disable @typescript-eslint/no-explicit-any */
import { login } from 'tplink-cloud-api';
import LB100 from 'tplink-cloud-api/distribution/lb100';
import { LampsService } from './lamps.service';
import { Logger } from '../../logging';

export const tplinkCloudLampsServiceFactory = async ({
  username,
  password,
  logger,
}: {
  username: string;
  password: string;
  logger: Logger;
}): Promise<LampsService> => {
  const tplink = await login(username, password);

  const bulbs: LB100[] = [];

  await tplink.getDeviceList();

  bulbs.push(tplink.getLB110('livingRoom-allo'));
  bulbs.push(tplink.getLB100('livingRoom-olv'));
  bulbs.push(tplink.getLB100('livingRoom-kisallo'));

  const setLamps = async (groups: string[], level: number): Promise<void> => {
    try {
      await Promise.all(
        bulbs.map(async (bulb) => {
          await (bulb as any).setState(1);
          return await (bulb as any).setState(1, level);
        }),
      );
    } catch (error) {
      return logger.error(error);
    }
  };

  const off = async (groups: string[], level: number): Promise<void> => {
    try {
      await Promise.all(
        bulbs.map(async (bulb) => {
          return await (bulb as any).setState(0, level);
        }),
      );
    } catch (error) {
      return logger.error(error);
    }
  };

  return { setLamps, off };
};
