import { Intent, IntentOptions } from '../intent/intent';

export type Job = {
  id: Intent;
  run: (options: IntentOptions) => Promise<void>;
  shouldRunEveryRound?: boolean;
};
