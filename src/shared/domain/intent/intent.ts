export enum Intent {
  SYNC_DEVICES = 'syncDevices',
  LAMPS_ON = 'lampsOn',
  LAMPS_OFF = 'lampsOff',
  MORNING = 'morning',
  SET_LAMPS = 'setLamps',
  SLEEP = 'sleep',
  START_NETFLIX = 'startNetflix',
  EXIT_NETFLIX = 'exitNetflix',
  START_HBOGO = 'startHBOGO',
  EXIT_HBO = 'exitHBO',
  START_TUNEIN = 'startTunein',
  EXIT_TUNEIN = 'exitTunein',
  VOLUME_UP = 'volumeUp',
  VOLUME_DOWN = 'volumeDown',
  EXIT_TV_PROGRAM = 'exitTvProgram',
  MUTE_TV = 'muteTv',
}

export type IntentOptionsLevel = {
  level: {
    level: number;
  };
};
export type IntentOptions = IntentOptionsLevel | {};
export const isIntentOptionsLevel = (options: IntentOptions): options is IntentOptionsLevel =>
  !!(options as IntentOptionsLevel).level;
