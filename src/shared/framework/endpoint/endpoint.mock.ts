import { Endpoint, EndpointMethod } from './endpoint';
import { APIError } from '../errors/errors';

export const createEndpointMock = ({
  error,
  type,
  status = 200,
  response = {},
}: {
  status?: number;
  response?: object;
  error?: Error | APIError;
  type?: string;
} = {}): Endpoint<{}, {}, {}, {}, {}> => ({
  method: EndpointMethod.GET,
  route: '/mock',
  schema: {
    tags: ['mock'],
  },
  handler: () =>
    error
      ? Promise.reject(error)
      : Promise.resolve({
          type,
          status,
          response,
        }),
});
