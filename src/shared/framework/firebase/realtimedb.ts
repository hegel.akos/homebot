import firebase from 'firebase';
import { FirebaseConfig } from '../../config';

export const createRealtimeDbRef = ({ config, ref }: { config: FirebaseConfig; ref: string }) => {
  firebase.initializeApp(config);
  const db = firebase.database();
  return {
    lamps: db.ref(`${ref}/lamps`),
  };
};
