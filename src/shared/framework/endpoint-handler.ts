import { Handler } from 'aws-lambda';

export type EndpointHandler<Event = unknown, Response extends { body: string } = { body: string }> = Handler<
  Event,
  Response
>;

export type HandlerEvent<
  Parameters extends object = {},
  Body extends object = {},
  Headers extends { [key: string]: string | string[] } = {}
> = {
  pathParameters: Parameters;
  body: Body;
  headers: Headers;
};
