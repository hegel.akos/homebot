import { config } from '../config';
export const dynamoConfigFactory = ({
  region = config.aws.dynamodb.region,
  accessKeyId = config.aws.dynamodb.accessKeyId,
  secretAccessKey = config.aws.dynamodb.secretAccessKey,
  endpoint = config.aws.dynamodb.endpoint,
}: Partial<{
  region: string;
  accessKeyId: string;
  secretAccessKey: string;
  endpoint: string;
}> = {}): { region: string; accessKeyId: string; secretAccessKey: string; endpoint: string } => ({
  region,
  accessKeyId,
  secretAccessKey,
  endpoint,
});
