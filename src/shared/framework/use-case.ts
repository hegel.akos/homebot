export type UseCase<Input, Output> = (_: Input) => Promise<Output>;
