export class APIError {
  constructor(public status: number, public detail: string) {}
}
