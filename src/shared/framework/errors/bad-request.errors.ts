import { APIError } from './errors';

export class BadRequest extends APIError {
  constructor(detail: string) {
    super(400, detail);
  }
}
