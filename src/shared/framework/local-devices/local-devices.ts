import find from 'local-devices';
export default (() => {
  return { find };
})();

export type LocalDevices = { find: () => Promise<Device[]> };

export type Device = find.IDevice;
