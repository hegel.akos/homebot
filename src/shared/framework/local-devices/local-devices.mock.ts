import { LocalDevices } from './local-devices';

export default function ({ findResult = [] } = {}) {
  const localDevicesMock = {
    find: () => {
      return;
    },
  } as LocalDevices;

  const spies = { findSpy: spyOn(localDevicesMock, 'find').and.returnValue(Promise.resolve(findResult)) };

  return {
    ...spies,
    localDevices: localDevicesMock,
  };
}
