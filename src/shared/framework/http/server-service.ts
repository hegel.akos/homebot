export interface ServerService {
  start(port: number, host: string, cb: () => void): void;
}
