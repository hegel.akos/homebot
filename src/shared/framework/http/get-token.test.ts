import { getTokenFromHeader } from './get-token';

describe('get token', () => {
  test('should retrieve empty string if key not found', () => {
    const exampleHeaders: { [key: string]: string | string[] } = {
      fake: 'Bearer token',
    };
    const token = getTokenFromHeader(exampleHeaders);
    expect(token).toBe('');
  });

  test('should retrieve empty string if value is array', () => {
    const exampleHeaders: { [key: string]: string | string[] } = {
      authorization: ['Bearer token'],
    };
    const token = getTokenFromHeader(exampleHeaders);
    expect(token).toBe('');
  });

  test('should retrieve empty string if value is not starting with the given prefix', () => {
    const exampleHeaders: { [key: string]: string | string[] } = {
      authorization: 'fake token',
    };
    const token = getTokenFromHeader(exampleHeaders);
    expect(token).toBe('');
  });

  test('should retrieve the token without the prefix', () => {
    const exampleHeaders: { [key: string]: string | string[] } = {
      authorization: 'Bearer token',
    };
    const token = getTokenFromHeader(exampleHeaders);
    expect(token).toBe('token');
  });
});
