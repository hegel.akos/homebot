import { APIError } from '../errors/errors';
import { createLoggerMock } from '../logger/logger.mock';
import { createErrorMapper } from './error-mapper';

describe('fastify error mapper', () => {
  test('should return status 500 and error if it was not instanceof APIError', () => {
    const error = new Error('unexpected error');
    const errorMapper = createErrorMapper();
    const result = errorMapper(error, createLoggerMock());
    expect(result).toHaveProperty('status');
    expect(result).toHaveProperty('response');
    expect(result.response).toHaveProperty('detail');
    expect(result.status).toBe(500);
    expect((result.response as APIError)['detail']).toEqual(error);
  });

  test('should return the mapped error status and message with APIError instance', () => {
    class CustomError extends APIError {
      constructor(msg: string) {
        super(400, msg);
      }
    }
    const error = new CustomError('reason');
    const errorMapper = createErrorMapper();
    const result = errorMapper(error, createLoggerMock());
    expect(result).toHaveProperty('status');
    expect(result).toHaveProperty('response');
    expect(result.response).toHaveProperty('detail');
    expect(result.status).toBe(400);
    expect((result.response as APIError)['detail']).toBe('reason');
  });
});
