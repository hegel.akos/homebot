import { FastifyReply, FastifyRequest } from 'fastify';
import { Endpoint } from '../endpoint/endpoint';
import { Logger } from '../logger/logger';
import { ErrorMapper } from './error-mapper';

export const endpointWrapper = (endpoint: Endpoint, logger: Logger, errorMapper: ErrorMapper) => async (
  request: FastifyRequest,
  reply: FastifyReply,
) => {
  const start = Date.now();
  try {
    const endpointArgs = {
      client: {
        ip: request.ip,
        useragent: request.headers['user-agent'],
      },
      headers: request.headers,
      query: request.query,
      params: request.params,
      body: request.body,
    };
    const endpointResult = await endpoint.handler(endpointArgs as any);
    logger.info({
      method: request.method,
      route: endpoint.route,
      duration: Date.now() - start,
      status: endpointResult.status,
    });
    if (endpointResult.headers) {
      endpointResult.headers.map((header) => reply.header(header.name, header.value));
    }
    const replyContentType = endpointResult.type || 'application/json';

    return reply.type(replyContentType).status(endpointResult.status).send(endpointResult.response);
  } catch (err) {
    const errorResponse = errorMapper(err, logger);
    return reply.status(errorResponse.status).send(errorResponse.response);
  }
};
