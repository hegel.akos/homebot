import fastify, { FastifyInstance, RouteOptions } from 'fastify';
import * as fastifyCors from 'fastify-cors';
import { Endpoint } from '../endpoint/endpoint';
import { Logger } from '../logger/logger';
import { endpointWrapper } from './endpoint-wrapper';
import { ErrorMapper } from './error-mapper';

export const createFastifyServerService = (args: {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  endpoints: Endpoint<any, any, any, any, any>[];
  logger: Logger;
  cors: { allowedOrigins: string };
  errorMapper: ErrorMapper;
}): FastifyInstance => {
  const fastifyServer = fastify({
    logger: false,
  });

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  void fastifyServer.register(fastifyCors as any, {
    allowedOrigins: args.cors.allowedOrigins,
  });

  args.endpoints.map((endpoint) => {
    const fastifyRouteOptions: RouteOptions = {
      method: endpoint.method,
      url: endpoint.route,
      schema: endpoint.schema,
      handler: endpointWrapper(endpoint, args.logger, args.errorMapper),
    };
    fastifyServer.route(fastifyRouteOptions);
  });

  return fastifyServer;
};
