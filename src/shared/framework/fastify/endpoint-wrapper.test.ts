import { FastifyReply, FastifyRequest } from 'fastify';
import { createEndpointMock } from '../endpoint/endpoint.mock';
import { createLoggerMock } from '../logger/logger.mock';
import { endpointWrapper } from './endpoint-wrapper';
import { createErrorMapperMock } from './error-mapper.mock';

const fastifyReplyMock = ({
  type: () => fastifyReplyMock,
  status: () => fastifyReplyMock,
  send: () => fastifyReplyMock,
} as unknown) as FastifyReply;

const errorMapperMock = { errorMapper: createErrorMapperMock() };
const loggerMock = createLoggerMock();

describe('fastify endpoint wrapper', () => {
  test('should fall to default settings, 200, application/json with mock response', async () => {
    const errorMapperSpy = spyOn(errorMapperMock, 'errorMapper').and.returnValue(fastifyReplyMock);
    const typeSpy = spyOn(fastifyReplyMock, 'type').and.returnValue(fastifyReplyMock);
    const statusSpy = spyOn(fastifyReplyMock, 'status').and.returnValue(fastifyReplyMock);
    const sendSpy = spyOn(fastifyReplyMock, 'send').and.returnValue(fastifyReplyMock);
    const loggerSpy = spyOn(loggerMock, 'info');
    const handle = endpointWrapper(createEndpointMock(), loggerMock, errorMapperMock.errorMapper);
    await handle({} as FastifyRequest, fastifyReplyMock);
    expect(errorMapperSpy).not.toHaveBeenCalled();
    expect(typeSpy).toHaveBeenCalledTimes(1);
    expect(typeSpy).toHaveBeenCalledWith('application/json');
    expect(statusSpy).toHaveBeenCalledTimes(1);
    expect(statusSpy).toHaveBeenCalledWith(200);
    expect(sendSpy).toHaveBeenCalledTimes(1);
    expect(loggerSpy).toHaveBeenCalledTimes(1);
  });

  test('should use the given type as response type', async () => {
    const typeSpy = spyOn(fastifyReplyMock, 'type').and.returnValue(fastifyReplyMock);
    const handle = endpointWrapper(createEndpointMock({ type: 'test/type' }), loggerMock, errorMapperMock.errorMapper);
    await handle({} as FastifyRequest, fastifyReplyMock);
    expect(typeSpy).toHaveBeenCalledTimes(1);
    expect(typeSpy).toHaveBeenCalledWith('test/type');
  });

  test('should catch the error and return the given status and response', async () => {
    const statusSpy = spyOn(fastifyReplyMock, 'status').and.returnValue(fastifyReplyMock);
    const sendSpy = spyOn(fastifyReplyMock, 'send').and.returnValue(fastifyReplyMock);
    const handle = endpointWrapper(
      createEndpointMock({ error: new Error('unexpected error') }),
      loggerMock,
      errorMapperMock.errorMapper,
    );
    await handle({} as FastifyRequest, fastifyReplyMock);
    expect(statusSpy).toHaveBeenCalledTimes(1);
    expect(statusSpy).toHaveBeenCalledWith(200);
    expect(sendSpy).toHaveBeenCalledTimes(1);
    expect(sendSpy).toHaveBeenCalledWith({});
  });
});
