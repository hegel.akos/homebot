import { ErrorMapper } from './error-mapper';

export const createErrorMapperMock = ({
  status = 200,
  response = {},
}: {
  status?: number;
  response?: object;
} = {}): ErrorMapper => () => ({
  status,
  response,
});
