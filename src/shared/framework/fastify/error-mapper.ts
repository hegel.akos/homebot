import { APIError } from '../errors/errors';
import { Logger } from '../logger/logger';

export type ErrorMapper = (err: Error | APIError, logger: Logger) => { status: number; response: object };

export const createErrorMapper = (): ErrorMapper => (err, logger) => {
  if (err instanceof APIError) {
    return {
      status: err.status || 500,
      response: {
        detail: err.detail || err,
      },
    };
  }
  logger.error(err);
  return {
    status: 500,
    response: { detail: err },
  };
};
