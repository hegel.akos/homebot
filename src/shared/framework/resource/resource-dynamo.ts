import { Resource } from './resource';

export type ResourceDynamo = Resource & {
  createdAt: string;
  updatedAt: string;
};
