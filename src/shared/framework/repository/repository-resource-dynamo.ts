import AWS from 'aws-sdk';
import { Resource } from '../resource/resource';
import { ResourceDynamo } from '../resource/resource-dynamo';
import { CreateRespository, DeleteRepository, ReadRepository, UpdateRepository } from './repository-resource';

type CreateReposotoryFactoryArgs<ObjectType extends ResourceDynamo> = {
  client: AWS.DynamoDB.DocumentClient;
  tableName: string;
};
export const createRepositoryDynamoFactory = <ObjectType extends ResourceDynamo>(
  factoryArgs: CreateReposotoryFactoryArgs<ObjectType>,
): CreateRespository<ObjectType>['create'] => async (obj: ObjectType): Promise<ObjectType['id']> => {
  const params = {
    TableName: factoryArgs.tableName,
    Item: {
      ...obj,
    },
  };
  await factoryArgs.client.put(params).promise();
  return obj.id;
};

type ReadReposotoryFactoryArgs = {
  client: AWS.DynamoDB.DocumentClient;
  tableName: string;
};
export const readRepositoryDynamoFactory = <ObjectType extends ResourceDynamo>(
  factoryArgs: ReadReposotoryFactoryArgs,
): ReadRepository<ObjectType>['read'] => async (id: ObjectType['id']): Promise<ObjectType | null> => {
  const params = {
    TableName: factoryArgs.tableName,
    KeyConditionExpression: 'id = :i',
    ExpressionAttributeValues: {
      ':i': id,
    },
  };
  const response = await factoryArgs.client.query(params).promise();
  if (response.Items?.length) {
    return response.Items[0] as ObjectType;
  }
  return null;
};

type DeleteReposotoryFactoryArgs = {
  client: AWS.DynamoDB.DocumentClient;
  tableName: string;
};
export const deleteRepositoryDynamoFactory = <IDType = Resource['id']>(
  factoryArgs: DeleteReposotoryFactoryArgs,
): DeleteRepository<IDType>['delete'] => async (id: IDType): Promise<void> => {
  const params = {
    TableName: factoryArgs.tableName,
    Key: {
      id,
    },
  };
  await factoryArgs.client.delete(params).promise();
};

type UpdateReposotoryFactoryArgs<T extends { [key: string]: unknown }> = {
  client: AWS.DynamoDB.DocumentClient;
  tableName: string;
};
export const updateRepositoryDynamoFactory = <ObjectType extends { [key: string]: unknown }, IDType = Resource['id']>(
  factoryArgs: UpdateReposotoryFactoryArgs<ObjectType>,
): UpdateRepository<ObjectType, IDType>['update'] => async (id: IDType, updates: ObjectType): Promise<void> => {
  const keys = Object.keys(updates);
  const updateExpression = keys.reduce((acc, key) => {
    if (keys.indexOf(key) === keys.length - 1) {
      return `${acc} #${key} = :${key}`;
    }
    return `${acc} #${key} = :${key}, `;
  }, 'SET ');
  const expressionAttributeNames = keys.reduce((acc: { [key: string]: string }, key) => {
    acc[`#${key}`] = key;
    return acc;
  }, {});

  const updateExpressionValues = keys.reduce((acc: { [key: string]: unknown }, key: string) => {
    acc[`:${key}`] = updates[key];
    return acc;
  }, {});
  const params = {
    TableName: factoryArgs.tableName,
    UpdateExpression: updateExpression,
    ExpressionAttributeValues: updateExpressionValues,
    ExpressionAttributeNames: expressionAttributeNames,
    Key: {
      id,
    },
    ReturnValues: 'UPDATED_NEW',
  };
  await factoryArgs.client.update(params).promise();
};
