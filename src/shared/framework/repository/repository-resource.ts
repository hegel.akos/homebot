import { Resource } from '../resource/resource';

export interface CreateRespository<ObjectType extends Resource> {
  create(obj: ObjectType): Promise<ObjectType['id']>;
}
export interface ReadRepository<ObjectType extends Resource> {
  read(id: ObjectType['id']): Promise<ObjectType | null>;
}
export interface UpdateRepository<ObjectType extends Omit<Resource, 'id'>, IDType = Resource['id']> {
  update(id: IDType, updates: ObjectType): Promise<void>;
}
export interface DeleteRepository<IDType = Resource['id']> {
  delete(id: IDType): Promise<void>;
}
