export const responseBodyFormatter = (
  obj: object,
): { body: string; headers: { [key: string]: string | string[] | boolean } } => ({
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': true,
  },
  body: JSON.stringify(obj),
});
