import { responseBodyFormatter } from './body-formatter';
import { EndpointHandler } from './endpoint-handler';

export const handlerWrapper = <Event, Response extends { body: string }>(
  endpointHandler: EndpointHandler<Event, Response>,
): EndpointHandler<Event, Response> => async (event, ctx, cb) => {
  try {
    const parsedBody = JSON.parse(((event as unknown) as { body: string }).body);
    const result = await endpointHandler(({ ...event, body: parsedBody } as unknown) as Event, ctx, cb);
    return result as Response;
  } catch (err) {
    console.error(err);
    if (err.message === 'not found') {
      return (responseBodyFormatter({ error: 'not found' }) as unknown) as Promise<Response>;
    }
    if (err.message === 'validate') {
      return (responseBodyFormatter({ error: 'validation error' }) as unknown) as Promise<Response>;
    }
    return (responseBodyFormatter({ errorString: err.toString(), err }) as unknown) as Promise<Response>;
  }
};
