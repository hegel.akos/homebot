import { Logger } from './logger';

export const createLoggerMock = (): Logger => ({
  info: () => {
    return;
  },
  debug: () => {
    return;
  },
  error: () => {
    return;
  },
  warn: () => {
    return;
  },
});
