import convict from 'convict';
import * as dotenv from 'dotenv';

dotenv.config();
dotenv.config({ path: `.env.${process.env.NODE_ENV}` });

export type AWSConfig = {
  dynamodb: {
    region: string;
    accessKeyId: string;
    secretAccessKey: string;
    apiVersion: string;
    endpoint: string;
  };
};

export type KasaConfig = {
  username: string;
  password: string;
};

export type MongoDBConfig = {
  connectionString: string;
};

export type FirebaseConfig = {
  apiKey: string;
  authDomain: string;
  databaseURL: string;
  projectId: string;
  storageBucket: string;
  messagingSenderId: string;
  appId: string;
  measurementId: string;
};

export type LoggerConfig = {
  level: string;
};

export type CorsConfig = {
  allowedOrigins: string;
};

export type ConfigObject = {
  port: number;
  logger: LoggerConfig;
  cors: CorsConfig;
  mongo: MongoDBConfig;
  aws: AWSConfig;
  kasa: KasaConfig;
  firebase: FirebaseConfig;
};

const configObject = convict<ConfigObject>({
  port: {
    doc: 'The port of the server',
    format: Number,
    default: 8080,
    env: 'PORT',
  },
  cors: {
    allowedOrigins: {
      doc: 'The list of client origins in JSON format. Used for setting the Access-Control-Allow-Origin header.',
      format: String,
      default: '["http://localhost:3001"]',
      env: 'ALLOWED_CLIENT_ORIGINS',
    },
  },

  logger: {
    level: {
      doc: 'The level of the logger',
      format: String,
      default: 'info',
      env: 'LOGGER_LEVEL',
    },
  },
  mongo: {
    connectionString: {
      doc: 'Connection string for the mongo cluster',
      format: String,
      default: 'mongodb://127.0.0.1:27017',
      env: 'MONGO_CONNECTION_STRING',
    },
  },
  aws: {
    dynamodb: {
      region: {
        doc: 'Region name of dynamodb',
        format: String,
        default: 'us-west-1',
        env: 'DYNAMO_REGION',
      },
      accessKeyId: {
        doc: 'Access key for dynamodb',
        format: String,
        default: 'accessKeyId',
        env: 'DYNAMO_ACCESS_KEY',
      },
      secretAccessKey: {
        doc: 'Secret key for dynamodb',
        format: String,
        default: 'secretAccessKey',
        env: 'DYNAMO_SECRET_KEY',
      },
      apiVersion: {
        doc: 'API version of dynamodb',
        format: String,
        default: '2012-08-10',
        env: 'DYNAMO_API_VERSION',
      },
      endpoint: {
        doc: 'Endpoint for dynamodb',
        format: String,
        default: 'http://localhost:8000',
        env: 'DYNAMO_ENDPOINT',
      },
    },
  },
  kasa: {
    username: {
      doc: 'Username for kasa account',
      format: String,
      default: 'user',
      env: 'KASA_USERNAME',
    },
    password: {
      doc: 'Password for kasa account',
      format: String,
      default: 'password',
      env: 'KASA_PASSWORD',
    },
  },
  firebase: {
    apiKey: {
      doc: 'apiKey for firebase',
      format: String,
      default: 'firebase_apikey',
      env: 'FIREBASE_APIKEY',
    },
    authDomain: {
      doc: 'authDomain for firebase',
      format: String,
      default: 'firebase_authdomain',
      env: 'FIREBASE_AUTHDOMAIN',
    },
    databaseURL: {
      doc: 'databaseURL for firebase',
      format: String,
      default: 'firebase_databaseurl',
      env: 'FIREBASE_DATABASE_URL',
    },
    projectId: {
      doc: 'projectId for firebase',
      format: String,
      default: 'firebase_projectId',
      env: 'FIREBASE_PROJECT_ID',
    },
    storageBucket: {
      doc: 'storageBucket for firebase',
      format: String,
      default: 'firebase_storageBucket',
      env: 'FIREBASE_STORAGE_BUCKET',
    },
    messagingSenderId: {
      doc: 'messagingSenderId for firebase',
      format: String,
      default: 'firebase_messagingSenderId',
      env: 'FIREBASE_MESSAGE_SENDER_ID',
    },
    appId: {
      doc: 'appId for firebase',
      format: String,
      default: 'firebase_appId',
      env: 'FIREBASE_APP_ID',
    },
    measurementId: {
      doc: 'measurementId for firebase',
      format: String,
      default: 'firebase_measurementId',
      env: 'FIREBASE_MEASUREMENT_ID',
    },
  },
});

configObject.validate({ allowed: 'warn' });

export const config: ConfigObject = configObject.getProperties();
