import { config } from '../shared/config';
import { createLogger } from '../shared/framework/logger/logger-pino';
import { createApp } from './app';

const thrower = (err: unknown): void => {
  throw err;
};

const throwToGlobal = (err: unknown): NodeJS.Immediate => setImmediate(() => thrower(err));

const startApp = async (): Promise<void> => {
  const logger = createLogger({
    name: 'fastify-backend',
    version: '1.0.0',
    level: config.logger.level,
  });

  const server = createApp({ logger, config });

  server.start(config.port, '0.0.0.0', () => {
    logger.info(`server started on port ${config.port}`);
  });
};

startApp().catch(throwToGlobal);
