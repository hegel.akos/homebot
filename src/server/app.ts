import { createErrorMapper } from '../shared/framework/fastify/error-mapper';
import { createFastifyServerService } from '../shared/framework/fastify/fastify-server';
import { createRealtimeDbRef } from '../shared/framework/firebase/realtimedb';
import { Logger } from '../shared/logging';
import { ConfigObject } from './../shared/config';
import { createAddJobEndpoint } from './endpoints/add-job.endpoint';

export const createApp = ({ logger, config }: { config: ConfigObject; logger: Logger }) => {
  const realtimeDbRef = createRealtimeDbRef({ ref: 'jobs', config: config.firebase });

  const addJobEndpoint = createAddJobEndpoint({ dbRef: realtimeDbRef.lamps });

  const endpoints = [addJobEndpoint];

  const fastifyServer = createFastifyServerService({
    logger,
    endpoints,
    errorMapper: createErrorMapper(),
    cors: { allowedOrigins: config.cors.allowedOrigins },
  });

  return {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    start: (port: number, host: string, cb: any) => {
      fastifyServer.listen(port, host, cb);
    },
  };
};
