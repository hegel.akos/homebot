import firebase from 'firebase';
import { Endpoint, EndpointMethod } from '../../shared/framework/endpoint/endpoint';

type Body = {};
type Query = {};
type Params = {};
type Response = {};
type AddJobEndpointFactoryArgs = {
  dbRef: firebase.database.Reference;
};
type AddJobEndpoint = Endpoint<Body, Query, Params, Response>;
export const createAddJobEndpoint = (factoryArgs: AddJobEndpointFactoryArgs): AddJobEndpoint => ({
  method: EndpointMethod.POST,
  route: '/jobs',
  schema: {
    body: {
      type: 'object',
      additionalProperties: true,
    },
  },
  handler: async (request) => {
    console.log(request.body);
    const currentDate = new Date().toISOString().substring(0, 10);
    const dailyDbRef = factoryArgs.dbRef.child(currentDate);
    await dailyDbRef.push({ jobId: 'lampsOn', done: false, options: {} });
    return { status: 200, response: {} };
  },
});
type createAddJobsEndpoint = Endpoint<{}, {}, {}, {}, {}>;
