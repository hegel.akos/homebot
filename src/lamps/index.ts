import { config } from '../shared/config';
import { Job } from '../shared/domain/job/job';
import { tplinkCloudLampsServiceFactory } from '../shared/domain/lamps/lamps-tplink.service';
import { initJobsFirebaseScheduler } from '../shared/domain/scheduler/firebase-scheduler';
import { createRealtimeDbRef } from '../shared/framework/firebase/realtimedb';
import { loggerFactory } from '../shared/logging';
import { lampsOffJobFactory } from './jobs/lamps-off-job';
import { lampsOnJobFactory } from './jobs/lamps-on-job';
import { setLampsJobFactory } from './jobs/set-lamps-job';

const thrower = (err: unknown): void => {
  throw err;
};

const throwToGlobal = (err: unknown): NodeJS.Immediate => setImmediate(() => thrower(err));

process.on('unhandledRejection', throwToGlobal);

const startProgram = async (): Promise<void> => {
  const logger = loggerFactory({ version: '1.0.0', name: 'lights service logger', level: 'info' });

  const lampsService = await tplinkCloudLampsServiceFactory({
    username: config.kasa.username,
    password: config.kasa.password,
    logger,
  });

  const lampsOffJob = lampsOffJobFactory({
    lampsService,
  });

  const lampsOnJob = lampsOnJobFactory({
    lampsService,
  });

  const setLampsJob = setLampsJobFactory({
    lampsService,
  });

  const jobs: Job[] = [lampsOffJob, lampsOnJob, setLampsJob];

  const realtimeDbRef = createRealtimeDbRef({
    ref: 'jobs/lamp',
    config: config.firebase,
  });

  initJobsFirebaseScheduler({ jobs, dbRef: realtimeDbRef.lamps });

  logger.info('lights program started');
};

startProgram().catch(throwToGlobal);
