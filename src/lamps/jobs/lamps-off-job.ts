import { Intent } from '../../shared/domain/intent/intent';
import { Job } from '../../shared/domain/job/job';
import { LampsService } from '../../shared/domain/lamps/lamps.service';

export const lampsOffJobFactory = ({ lampsService }: { lampsService: LampsService }): Job => ({
  id: Intent.LAMPS_OFF,
  run: () => {
    return lampsService.off([], 0);
  },
});
