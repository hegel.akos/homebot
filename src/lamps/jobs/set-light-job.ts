import { Job } from '../../shared/domain/job/job';
import { LampsService } from '../../shared/domain/lamps/lamps.service';
import { Intent, IntentOptions, isIntentOptionsLevel } from '../../shared/domain/intent/intent';

export const setLightsJobFactory = ({ lightsService }: { lightsService: LampsService }): Job => ({
  id: Intent.SET_LAMPS,
  run: async (options: IntentOptions) => {
    if (isIntentOptionsLevel(options)) {
      return lightsService.setLamps([], options.level.level);
    }
  },
});
