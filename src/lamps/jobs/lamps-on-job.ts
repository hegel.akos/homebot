import { Intent } from '../../shared/domain/intent/intent';
import { Job } from '../../shared/domain/job/job';
import { LampsService } from '../../shared/domain/lamps/lamps.service';

export const lampsOnJobFactory = ({ lampsService }: { lampsService: LampsService }): Job => ({
  id: Intent.LAMPS_ON,
  run: () => {
    return lampsService.setLamps([], 100);
  },
});
