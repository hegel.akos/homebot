import { Job } from '../../shared/domain/job/job';
import { LampsService } from '../../shared/domain/lamps/lamps.service';
import { Intent, IntentOptions, isIntentOptionsLevel } from '../../shared/domain/intent/intent';

export const setLampsJobFactory = ({ lampsService }: { lampsService: LampsService }): Job => ({
  id: Intent.SET_LAMPS,
  run: async (options: IntentOptions) => {
    if (isIntentOptionsLevel(options)) {
      return lampsService.setLamps([], options.level.level);
    }
  },
});
